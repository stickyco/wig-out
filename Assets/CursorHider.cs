﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorHider : MonoBehaviour {

	public bool showCursor;

	private bool lastCursor;

	// Use this for initialization
	void Start () {
		lastCursor = showCursor;
		Cursor.visible = showCursor;
	}
	
	// Update is called once per frame
	void Update () {
		if (showCursor != lastCursor) {
			Cursor.visible = showCursor;
		}
		lastCursor = showCursor;
	}
}
