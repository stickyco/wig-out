using UnityEngine;

using com.rfilkov.kinect;

/// <summary>
/// Interaction manager is the component that deals with hand interactions.
/// 
/// Originally derived from Kinectv2 example HandInteractionManager which provided clicking and dragging
/// by hand gestures.
/// </summary>
public class HeadInteractionManager : MonoBehaviour 
{

	public delegate void UserChanged(bool userDetected);
	public static event UserChanged OnUserChanged;

	[Tooltip("Index of the player, tracked by this component. 0 means the 1st player, 1 - the 2nd one, 2 - the 3rd one, etc.")]
	public int playerIndex = 0;

	[Tooltip("Smooth factor for cursor movement.")]
	public float smoothFactor = 3f;

	[Tooltip("Whether head movement controls the mouse cursor or not.")]
	public bool headControlsMouseCursor = true;

	[Tooltip("Whether hand grips and releases control mouse dragging or not.")]
	public bool controlMouseDrag = false;

	[Tooltip("Max distance to left (0.0 is centered)")]
	public float headLeftMax = -0.3f;

	[Tooltip("Max distance to right (0.0 is centered)")]
	public float headRightMax = 0.3f;

	// Bool to specify whether to convert Unity screen coordinates to full screen mouse coordinates
	//public bool convertMouseToFullScreen = false;

	[Tooltip("GUI-Text to display the interaction-manager debug messages.")]
	public GUIText debugText;

	private ulong liPrimaryUserID = 0;
	private ulong lastPrimaryId = 0;

	private Vector3 cursorScreenPos = Vector3.zero;
	private bool dragInProgress = false;

	private Vector3 headPos = Vector3.zero;
	private Vector3 headScreenPos = Vector3.zero;
	private bool isHeadTracked = false;

	private Vector3 lastHeadPos = Vector3.zero;
	private float lastHeadTime = 0f;
	private bool isHeadClick = false;
	private float headClickProgress = 0f;
	
	// Bool to keep track whether Kinect and Interaction library have been initialized
	private bool interactionInited = false;

	// The single instance of FacetrackingManager
	private static HeadInteractionManager instance;


	/// <summary>
	/// Gets the single InteractionManager instance.
	/// </summary>
	/// <value>The InteractionManager instance.</value>
	public static HeadInteractionManager Instance
    {
        get
        {
            return instance;
        }
    }
	
	/// <summary>
	/// Determines whether the InteractionManager was successfully initialized.
	/// </summary>
	/// <returns><c>true</c> if InteractionManager was successfully initialized; otherwise, <c>false</c>.</returns>
	public bool IsInteractionInited()
	{
		return interactionInited;
	}

	/// <summary>
	/// Gets the current user ID, or 0 if no user is currently tracked.
	/// </summary>
	/// <returns>The user ID</returns>
	public ulong GetUserID()
	{
		return liPrimaryUserID;
	}

	public bool IsHeadTracked() 
	{
		return isHeadTracked;
	}
	
	/// <summary>
	/// Gets the current cursor normalized viewport position.
	/// </summary>
	/// <returns>The cursor viewport position.</returns>
	public Vector3 GetCursorPosition()
	{
		return cursorScreenPos;
	}


	//----------------------------------- end of public functions --------------------------------------//


	void Start() 
	{
		instance = this;
		interactionInited = true;
	}

	void OnDestroy()
	{
		// uninitialize Kinect interaction
		if(interactionInited)
		{
			interactionInited = false;
			instance = null;
		}
	}

	void Update () 
	{
		KinectManager kinectManager = KinectManager.Instance;

		// update Kinect interaction
		if(kinectManager && kinectManager.IsInitialized())
		{
			liPrimaryUserID = kinectManager.GetUserIdByIndex(playerIndex);

			if(liPrimaryUserID != 0)
			{

				// Update head state
				if (kinectManager.GetJointTrackingState(liPrimaryUserID, (int)KinectInterop.JointType.Head) != KinectInterop.TrackingState.NotTracked) {
					headPos = kinectManager.GetJointPosition(liPrimaryUserID, (int)KinectInterop.JointType.Head);

					headScreenPos.x = Mathf.Clamp01((headPos.x - headLeftMax) / (headRightMax - headLeftMax));
					headScreenPos.y = 0.3f;
					headScreenPos.z = 0;
				}

				isHeadTracked = kinectManager.IsJointTracked (liPrimaryUserID, (int)KinectInterop.JointType.Head);
											
			 	// process head
				{
					float smooth = smoothFactor * Time.deltaTime;
					if (smooth == 0f)
						smooth = 1f;
					cursorScreenPos = Vector3.Lerp (cursorScreenPos, headScreenPos, smooth);
				}

			}
		}
		if (liPrimaryUserID != lastPrimaryId) {
			if (HeadInteractionManager.OnUserChanged != null) {
				HeadInteractionManager.OnUserChanged (liPrimaryUserID != 0);
			}
			lastPrimaryId = liPrimaryUserID;
		}

	}

	void OnGUI()
	{
		if(!interactionInited)
			return;

		// display debug information
		if(debugText)
		{
			string sGuiText = string.Empty;

			{
				sGuiText += "\nHead: " + (isHeadTracked ? headScreenPos.ToString() : "UNTRACKED");
			}

			debugText.GetComponent<GUIText>().text = sGuiText;
		}
	}

}
