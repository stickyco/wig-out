using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour
{

	private enum GameState {INITIALIZING, WAITING, BEGINNING, PLAYING, RESTARTING, ENDING }

	public PlayerController playerController;
	public GameObject[] wigs;
	public GameObject[] enemies;
	public Text scoreText;
	public Text readyText;
	public Text highScoreText;
	public Canvas UICanvas;
	public Text WigScoreText;

	public AudioClip death;
	public AudioSource audio;
	public AudioListener audioOut;

	public Vector2 spawnValues;

	private static string HIGH_SCORE_LABEL = "BEST: ";
	private int wigsPerWave = 5;
	private float spawnWait = 0.75f;
	private float waveWait = 2.0f;

	private float enemyWaitMax = 5.0f;
	private float enemyWaitMin = 1.1f;
	private float enemyWaitDelta = 0.15f;
	private float enemyWait;
	private int score = 0;
	private int highScore = 0;
	private const int STARTING_SCORE_TIER = 200;
	private const int TIER_STEP_UP = 250;
	private int nextScoreTier = STARTING_SCORE_TIER;

	private GameState gameState;

	private Coroutine wigSpawnRoutine;
	private Coroutine enemySpawnRoutine;
	private Coroutine restartGameRoutine;
	private Coroutine beginRoutine;

	void Start () {
		PlayerController.OnPlayerChanged += OnPlayerChanged;
		PlayerController.OnPlayerCollide += onPlayerCollide;
		gameState = GameState.INITIALIZING;

		audio.clip = death;
		BeginWaiting ();
	}

	void BeginWaiting () {
		Debug.Log ("Beginnign wait.");
		score = 0;
		UpdateScore ();
		scoreText.enabled = false;
		readyText.enabled = false;
		playerController.showPlayer (false);
		playerController.enablePhysics (false);
		if (wigSpawnRoutine == null) {
			wigSpawnRoutine = StartCoroutine (SpawnWigs ());
		}
		gameState = GameState.WAITING;
		if (playerController.isUserActive ()) {
			beginRoutine = StartCoroutine (BeginTurn ());
		}
	}

	IEnumerator BeginTurn() {
		Debug.Log ("Beginnign turn.");
		nextScoreTier = STARTING_SCORE_TIER;
		gameState = GameState.BEGINNING;
		enemyWait = enemyWaitMax;
		scoreText.enabled = true;
		gameState = GameState.PLAYING;
		Debug.Log ("Playingn.");
		playerController.enablePhysics (true);
		playerController.showPlayer (true);
		playerController.Reset ();
		enemySpawnRoutine = StartCoroutine (SpawnEnemies ());
		readyText.enabled = false;
		yield return null;
	}

	IEnumerator SpawnWigs () {
		while (true) {
			for (int i = 0; i < wigsPerWave; i++) {
				GameObject newWig = wigs[Random.Range (0, wigs.Length)];
				Vector2 spawnPosition = new Vector2 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y);
				Instantiate (newWig, spawnPosition, transform.rotation);
				yield return new WaitForSeconds (spawnWait);
			}
			yield return new WaitForSeconds (waveWait);
		}
	}

	IEnumerator SpawnEnemies () {
		while (true) {
			if (enemyWait > enemyWaitMin) {
				enemyWait -= enemyWaitDelta;
			}
			GameObject newEnemy = enemies[Random.Range (0, enemies.Length)];
			// Maybe there's a null value in the array.
			while (newEnemy == null) {
			  newEnemy = enemies[Random.Range (0, enemies.Length)];
			}
			Vector2 spawnPosition = new Vector2 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y);
			Instantiate (newEnemy, spawnPosition, transform.rotation);
			yield return new WaitForSeconds (enemyWait);
		}
	}
				
	IEnumerator RestartGame () {
		Debug.Log ("Restarting");
		gameState = GameState.RESTARTING;
		yield return new WaitForSeconds (3f);
		BeginWaiting ();
	}

	public void EndTurn() {
		Debug.Log ("Ending turn.");
		gameState = GameState.ENDING;

		checkHiScore ();

		if (restartGameRoutine != null) {
			StopCoroutine (restartGameRoutine);
		}
		if (enemySpawnRoutine != null) {
			StopCoroutine (enemySpawnRoutine);
		}

		GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");
		foreach (GameObject enemy in enemies) {
			Destroy (enemy);
		}

        restartGameRoutine = StartCoroutine (RestartGame ());
    }

	void checkHiScore() {
		if (score > highScore) {
			highScore = score;
			highScoreText.text = HIGH_SCORE_LABEL + highScore.ToString();
			audio.Play ();
		}
	}

	void checkScoreProgress() {
		if (score > nextScoreTier) {
			audio.Play ();
			nextScoreTier += TIER_STEP_UP;
		}
	}

	IEnumerator AnimateScoreText(int newScoreValue, Vector3 newPosition) {
		Text scoreText = Instantiate(WigScoreText, newPosition, transform.rotation) as Text;
		scoreText.transform.SetParent(UICanvas.transform, false);
		scoreText.transform.position = new Vector3(newPosition.x, newPosition.y + 4.0f);
		scoreText.text = "+" + newScoreValue.ToString();
		yield return new WaitForSeconds (1f);
		Destroy (scoreText.gameObject);
	}

	void OnPlayerChanged(bool playerActive) {
		if (gameState == GameState.WAITING || gameState == GameState.PLAYING) {
			EndTurn ();
		}
	}

	void onPlayerCollide(GameObject obj) {
		if (obj.tag == "Wig") {
            DropController dc = obj.GetComponent<DropController>();
            dc.AnimateCollection();
            AddScore (dc.scoreValue);
            StartCoroutine(AnimateScoreText (dc.scoreValue, dc.gameObject.transform.position));
		} else if (obj.tag == "Enemy") {
			playerController.die ();
			EndTurn ();
		}
	}

	public void AddScore (int newScoreValue) {
		score += newScoreValue;
		checkScoreProgress ();
		UpdateScore ();
	}

	void UpdateScore () {
		scoreText.text = score.ToString();
	}
}
