﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropController : MonoBehaviour {

	public float velocity;
	public int scoreValue;

	private GameObject player;
	private bool isBeingCollectedByPlayer = false;
	private const int STEPS_TO_PLAYER = 20;
	private int steps;
	private const float OUT_OF_BOUNDS_Y = -15.0f;
	private float startTime;

	private Rigidbody2D rb2d;

	// Use this for initialization
	void Start () {
		startTime = Time.time;
		rb2d = GetComponent<Rigidbody2D> ();
		rb2d.gravityScale = -velocity;
		player = GameObject.FindWithTag ("Player");
	}
	
	void FixedUpdate () {
		if (isBeingCollectedByPlayer) {
			int inverseSteps = STEPS_TO_PLAYER - steps;
			float newX = (steps * transform.position.x + inverseSteps * player.transform.position.x) / STEPS_TO_PLAYER;
			float newY = (steps * transform.position.y + inverseSteps * player.transform.position.y) / STEPS_TO_PLAYER;
			transform.position = new Vector3(newX, newY, transform.position.y);
			transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.identity, 20.0f * Time.deltaTime);
			if (steps > 1) {
				steps--;
			}
		}
		else if (rb2d.position.y < OUT_OF_BOUNDS_Y) {
			Destroy (this.gameObject);
		}
	}
		
	public void AnimateCollection() {
		isBeingCollectedByPlayer = true;
		steps = STEPS_TO_PLAYER;
		Destroy (rb2d);
		Destroy (GetComponent<CircleCollider2D> ());
		StartCoroutine (MoveToPlayer ());
	}

	IEnumerator MoveToPlayer () {
		yield return new WaitForSeconds (0.5f);
		Destroy (gameObject);
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (Time.time - startTime < 0.1f) {
			if (gameObject.tag == "Enemy" && coll.gameObject.tag == "Enemy") {
				if (gameObject.name == "Anvil") {
					Destroy (gameObject);
				}
			}
		}
	}
}