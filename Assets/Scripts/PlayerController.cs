﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public delegate void PlayerChanged(bool playerActive);
	public static event  PlayerChanged OnPlayerChanged;
	public delegate void PlayerCollision(GameObject collected);
	public static event PlayerCollision OnPlayerCollide;

	public GameController gameController;
	public HeadInteractionManager kinect;
	public SpriteRenderer sprite;
	public float leftConstraint = -18.0f;
	public float rightConstraint = 18.0f;
	public float speed;
	public Sprite[] playerSprites;

	private bool isAlive;
	private bool kinectUserActive;
	private bool userActive;
	private Vector2 initialPosition;
	private Rigidbody2D rb2d;
	private int spriteIndex;

	void Start() {
		rb2d = GetComponent<Rigidbody2D> ();
		initialPosition = rb2d.position;
		kinectUserActive = false;
		showPlayer (false);
		setPlayerActive (false);
		HeadInteractionManager.OnUserChanged += OnUserChanged;

		spriteIndex = 0;
	}

	public void showPlayer(bool show) {
		if (show) {
			selectNextSprite ();
		}
		sprite.enabled = show;
	}

	private void selectNextSprite() {
		sprite.sprite = null;
		while (sprite.sprite == null) {
			spriteIndex += 1;
			spriteIndex = spriteIndex % playerSprites.Length;
			sprite.sprite = playerSprites [spriteIndex];
		}
	}

	public bool isUserActive () {
		return userActive;
	}

	public void enablePhysics(bool enabled) {
		rb2d.simulated = enabled;
	}

	void setPlayerActive(bool active) {
		bool wasActive = userActive;
		userActive = active;
		if (wasActive != userActive && OnPlayerChanged != null) {
			OnPlayerChanged (userActive);
		}
	}

	public void Reset() {
		rb2d.position = initialPosition;
		rb2d.velocity = new Vector2 (0f, 0f);
		rb2d.angularVelocity = 0f;
		rb2d.rotation = 0f;
		isAlive = true;
	}

	void FixedUpdate() {
		float moveHorizontal = Input.GetAxis ("Horizontal");
		if (moveHorizontal != 0.0) {
			setPlayerActive (true);
		}

		if (!isAlive) { return; }	

		rb2d.angularVelocity = 0f;

		if (kinectUserActive) {
			Vector2 newPos = rb2d.position;
			float width = Mathf.Abs (leftConstraint - rightConstraint);
			// Not accounting for player width because body/texture is anchored with 0,0 in its center.
			newPos.x = (kinect.GetCursorPosition ().x * width) - width / 2;
			newPos.y = initialPosition.y;
			rb2d.MovePosition (newPos);
			rb2d.velocity = new Vector2(0.0f, 0.0f);
			return;
		}

		bool bumpingEdge = false;
		Vector2 force = new Vector2 (0.0f, 0.0f);
		if (rb2d.position.x >= rightConstraint) {
			bumpingEdge = true;
			rb2d.angularVelocity = 0.0f;
			force.x = -10.0f;
		} else if (rb2d.position.x <= leftConstraint) {
			bumpingEdge = true;
			force.x = 10.0f;
		} else {
			Vector2 movement = new Vector2 (moveHorizontal, 0.0f);
			rb2d.AddForce (movement * speed);
		}

		if (bumpingEdge) {
			rb2d.velocity = new Vector2(0.0f, 0.0f);
			rb2d.angularVelocity = 0.0f;
		} else {
			force.x = moveHorizontal;
		}

		rb2d.AddForce (force * speed);
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (!userActive) {
			Debug.Log ("Ignoring collision, not active.");
			return;
		}

		if (OnPlayerCollide != null) {
			OnPlayerCollide (coll.gameObject);
		}
	}

	public void die() {
		isAlive = false;
		rb2d.velocity = Vector2.down * 15;
		rb2d.angularVelocity = 0.0f;
	}

	void OnUserChanged(bool userDetected) {
		bool stateChanged = userDetected != kinectUserActive;
		kinectUserActive = userDetected;
		setPlayerActive (userDetected);
		if (!userDetected) {
			die ();
		}
	}
}