﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotator : MonoBehaviour
{
	private const int AMOUNT = 10;

	void Start () {
		GetComponent<Rigidbody2D> ().angularVelocity = Random.insideUnitCircle.x * AMOUNT;
	}

}